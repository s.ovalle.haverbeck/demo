package com.example.demo.controller;

import jakarta.websocket.server.PathParam;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/customer")
public class UserController {


    @GetMapping("/getAllUser")
    public String getAllUsers() {
        return "Hello, World!";
    }

    @GetMapping("/getUserById/{id}")
    public String getUserById(@PathVariable String id) {
        return "Hello, World! " + id;
    }

    @GetMapping("/getUserByQuery")
    public String getUserByQuery(@PathParam("name") String name) {
        return "Hello, World! " + name;
    }

}
