git remote add origin https://git-codecommit.us-east-1.amazonaws.com/v1/repos/demo
git commit -m "Fist Commit"

git push -u https://sebastian.ovalle-at-813055217157:2O8ggpYWslbmGWQGslQTbjeJO8lJ8pBdeZQ3XcNlMdYompvDyUYIWHRslLY=@git-codecommit.us-east-1.amazonaws.com/v1/repos/demo main

git init
repository_url=$(aws codecommit get-repository --repository-name catsdogs --query 'repositoryMetadata.cloneUrlHttp' --output text)
git remote add origin $repository_url
git add .
git commit -m "first import"
git push --set-upstream origin master


# Setup 
## 1.- Download the setup files, unzip and navigate to the setup directory.
```
curl -o setup.zip 'https://static.us-east-1.prod.workshops.aws/public/90a982f9-c552-4b5c-9aa0-deaaec53f669/static/downloads/setup.zip'
unzip setup.zip
```
## 2.- Set the AWS_DEFAULT_REGION variable with the default region for your account (specified in the instructions, e.g. us-east-1 in the example below):
```
echo 'export AWS_DEFAULT_REGION=us-east-1' >> ~/.bashrc
source ~/.bashrc
```


## 3.- Execute the setup.shscript to:

Install the required tools and set required environment variables (current Region, Account ID, Repository URI, etc.)
- Create initial IAM roles
- Create an ECR repository and push sample application container image
- Create the required infrastructure for the workshop:
- ECS cluster with Fargate
- Application Load Balancer (ALB)
- Task definition and Task role
- Networking: VPC, public and private subnets, NAT Gateway, route tables, security groups
- Sample application deployed
```
cd setup/
chmod +x setup.sh
./setup.sh
```

This process will take several minutes as it needs to deploy several resources which are dependent on each other in most cases, for example the ECS Service will not be created until the Application Load Balancer and its listener are created and configured.

# Define the build spec
 A build spec is a file were we specify the details of a build. In other words, a build spec is a collection of build commands and related settings in YAML format, that AWS CodeBuild uses to run a build.

Get the value for your AWS ECR repository

```
export AWS_ACCOUNT_ID=$(aws sts get-caller-identity --query Account --output text)
export ECR_REPOSITORY=$AWS_ACCOUNT_ID.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com/devops-workshop-app
echo $ECR_REPOSITORY
```
The latter is because with each new container image we get a new revision of the task definition, for example: if the revision was webapp:2, after pushing the newly built image we will have a new revision webapp:3. In normal conditions we would need to update the taskdefinition value in some way, most likely manually or with complex scripts. In this case CodeDeploy will do the magic for us.

```
version: 0.0
Resources:
- TargetService:
  Type: AWS::ECS::Service
  Properties:
  TaskDefinition: <TASK_DEFINITION>
  LoadBalancerInfo:
  ContainerName: "webapp"
  ContainerPort: 80
```

We can also achive this programatically through the command line running the following commands:

```
export TASK_DEFINITION=$(aws ecs list-task-definitions | jq -r '.taskDefinitionArns[0]')

aws ecs describe-task-definition --task-definition $TASK_DEFINITION \
--query "taskDefinition.{family:family, taskRoleArn:taskRoleArn, executionRoleArn:executionRoleArn, networkMode:networkMode, containerDefinitions:containerDefinitions, volumes:volumes, placementConstraints:placementConstraints, requiresCompatibilities:requiresCompatibilities, cpu:cpu, memory:memory, tags:tags, pidMode:pidMode, ipcMode:ipcMode, proxyConfiguration:proxyConfiguration}" | jq 'del(.[] | nulls)' > taskdef.json

```

pushed