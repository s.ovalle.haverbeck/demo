# Build stage
FROM maven:sapmachine AS build
WORKDIR /app
COPY pom.xml .
COPY src ./src
RUN mvn clean package -DskipTests

# Package stage
FROM openjdk:24-slim-bullseye
WORKDIR /app
COPY --from=build /app/target/*.jar app.jar
#COPY /app/target/*.jar app.jar
EXPOSE 8080
CMD ["java", "-jar", "app.jar"]